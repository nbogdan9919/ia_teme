# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def test(problem):
    # ex 2.6
    s = problem.getSuccessors(problem.getStartState());
    for state in s:
        print("Position", state[0], "direction", state[1], "Cost", state[2])

    "*** YOUR CODE HERE ***"

    # ex 2.7
    from game import Directions
    w = Directions.WEST;
    # return [w for _ in range(2)];

    # ex 2.8

    from util import CustomNode

    s1 = problem.getSuccessors(problem.getStartState())[0]
    s2 = problem.getSuccessors(s1[0])[0]

    # return( s1[1],s2[1]);

    # 2.9
    n1 = CustomNode("test1", 2)
    n2 = CustomNode("test2", 4)
    print("Noduriile sunt\n", n1, "\n", n2)
    myStack = util.Stack();
    myStack.push(n1);
    myStack.push(n2);

    res = myStack.pop();
    print("Pop la", res);

    return (s1[1], s2[1]);

#verific daca element e in PriorityQueue
def elemInPriorityQueue(elem, queue):
    for index, (p, c, i) in enumerate(queue.heap):
        if (elem[0] == i[0]):
            return True;
    return False;

#verific daca elementul e in queue
def elemInQueue(element, queue):
    for elem in queue.list:
        if elem[0] == element:
            return True;

    return False;


def depthFirstSearch(problem):
    explored = []
    start_node = problem.getStartState();
    struct = util.Stack();

    struct.push((start_node, [], 0))

    while not struct.isEmpty():

        node = struct.pop();

        explored.append(node[0])

        if problem.isGoalState(node[0]):
            return node[1]

        succ = problem.getSuccessors(node[0])

        for child in succ:
            if child[0] not in explored :   # and not elemInQueue(child[0],struct): solutie care nu respecta DFS insa e mai buna
                newPath = node[1] + [child[1]]

                cost =node[2]+child[2]; # nu se ia in considerare costul pt acest algoritm
                struct.push((child[0], newPath, cost))

                #if(problem.isGoalState(child[0])): #produce solutie mai buna insa nu se comporta
                    #return newPath;                # ca dfs, nu respecta ordinea de parcurgere
                                                    #ex daca avem  A-> Goal
                                                    #              |    |
                                                    #              B->  /
                                                    #Solutia gasita ar fi cea directa A->Goal
                                                    #insa DFS ar trebui sa mearga  A-> B -> Goal pt a respecta algoritmul
                                                    # question 1 , fail 2 (graph_bfs_vs_dfs)
                                                    # acesta este doar exemplul din autograder, in cazul in care din B se expandau
                                                    #mai multe noduri care intr-un final ajungeau la Goal, se alegea varianta aceea
                                                    # din cauza algoritmului
# 2.10
def randomSearch(problem):
    from game import Directions
    import random
    current = problem.getStartState();
    w = Directions.WEST;
    list = [];

    while (not (problem.isGoalState(current))):
        successors = problem.getSuccessors(current)
        nr_succs = len(successors)
        index = (int(random.random() * nr_succs))
        next = successors[index];
        current = next[0]
        list.append(next[1])

    print(list)
    return list;


def breadthFirstSearch(problem):
    explored = []
    start_node = problem.getStartState();
    struct = util.Queue();


    struct.push((start_node, [], 0))

    while not struct.isEmpty():

        node = struct.pop();

        explored.append(node[0])

        if problem.isGoalState(node[0]):
            return node[1]

        succ = problem.getSuccessors(node[0])

        for child in succ:
            if child[0] not in explored and not elemInQueue(child[0], struct):
                newPath = node[1] + [child[1]]
                cost = node[2]+child[2] # nu este necesar, nu se foloseste costul
                struct.push((child[0], newPath, cost))

                explored.append(child[0]);
                #if(problem.isGoalState(child[0])):  # solutie prea eficienta, nu se repecta BFS
                    #return newPath;                # deoarece nu se mai verifica toate noduriile de pe nivelul respectiv
                                                    # dupa ce s-a gasit solutia


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"

    start_node = problem.getStartState();
    explored = [];
    struct = util.PriorityQueue()
    struct.push([start_node, [], 0], 0);

    while not struct.isEmpty():

        node = struct.pop();

        explored.append(node[0]);

        if problem.isGoalState(node[0]):
            return node[1];

        successors = problem.getSuccessors(node[0]);

        for child in successors:

            newPath = node[1] + [child[1]];
            priority = problem.getCostOfActions(newPath);
            cost = node[2] + child[2];

            if child[0] not in explored and not elemInPriorityQueue(child[0], struct):
                struct.push((child[0], newPath, cost), priority);
                explored.append(child[0]);


            for index, (p, c, i) in enumerate(struct.heap): # verif daca exista deja in queue
                if (i[0] == child[0] and p > priority): # daca exista si are prioritate mai mare se face update
                    struct.update((child[0], newPath, cost), priority);

    return False;

    # util.raiseNotDefined()


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"

    start_node = problem.getStartState();
    explored = [];
    struct = util.PriorityQueue()
    struct.push([start_node, [], 0], 0);



    while not struct.isEmpty():

        node = struct.pop();

        explored.append(node[0]);



        if problem.isGoalState(node[0]):
            return node[1];

        successors = problem.getSuccessors(node[0]);


        for child in successors:

            newPath = node[1] + [child[1]];
            priority = problem.getCostOfActions(newPath) + heuristic(child[0], problem);
            cost=node[2]+child[2];



            if child[0] not in explored and not elemInPriorityQueue(child[0], struct):
                struct.push((child[0], newPath, cost), priority);
                explored.append(child[0]);

            for index, (p, c, i) in enumerate(struct.heap): #verif daca exista deja in queue
                if (i[0] == child[0] and p > priority): # daca are priority mai mare se schimba
                    struct.update((child[0], newPath, cost), priority);

    return False;


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
rs = randomSearch
