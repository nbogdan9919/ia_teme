(define (problem g)
(:domain game)

(:objects
s11 s12 s13        					
s21 s22 s23  front_house   outside_store inside_store   
s31 s32 s33  labirint_entrance
						l11 l12 l13     					
						l21 l22 l23 
						l31 l32 l33	
player

money
key


bike
car

shovel

weapon

guardLoc


treasure
safe_code
trapLoc
gameWinLoc

)

(:init
(right s11 s12)		(left s13 s12)    
(right s12 s13)		(left s12 s13)	  (left outside_store inside_store)
(right s21 s22)		(left s23 s22)
(right s22 s23)		(left s22 s21)
(right s31 s32)		(left s33 s32)
(right s32 s33)		(left s32 s31)

(up s31 s21)		(down s11 s21)
(up s21 s11)		(down s21 s31)
(up s32 s22)		(down s12 s22)
(up s22 s12)		(down s22 s32)
(up s33 s23)		(down s13 s23)
(up s23 s13)		(down s23 s33)

(right labirint_entrance l11)

(left l11 labirint_entrance)
(up labirint_entrance front_house)
(right l11 l12)		(left l13 l12)    
(right l12 l13)		(left l12 l13)	  (left outside_store inside_store)
(right l21 l22)		(left l23 l22)
(right l22 l23)		(left l22 l21)
(right l31 l32)		(left l33 l32)
(right l32 l33)		(left l32 l31)

(up l31 l21)		(down l11 l21)
(up l21 l11)		(down l21 l31)
(up l32 l22)		(down l12 l22)
(up l22 l12)		(down l22 l32)
(up l33 l23)		(down l13 l23)
(up l23 l13)		(down l23 l33)

(= (total-cost) 0)
(= (max_carry bike) 10)
(= (max_carry car) 50)

(= (carrying) 1)



(= (obj_weight shovel) 10)
(= (obj_weight door) 15)
(= (obj_weight money) 15)
(= (obj_weight key) 15)
(= (obj_weight player) 1)
(= (obj_weight bike) 5)
(= (obj_weight car) 5)
(= (obj_weight weapon) 10)
(= (obj_weight treasure) 10)
(= (obj_weight safe_code) 10)





(at player s31)
(at money s11)
(at bike front_house)
(at car front_house)
(at key s33)
(at shovel inside_store)
(at weapon l31)
(at safe_code l13)



(for_sale shovel)
(can_move player)


(door s23 front_house)

(can_unlock key)

(drivable bike)
(drivable car)


(road front_house outside_store bike)
(road front_house outside_store car)

(road  outside_store front_house bike)
(road  outside_store front_house car)

(can_trade money)

(leave_store inside_store outside_store)


(can_dig shovel)


(treasure_loc front_house labirint_entrance)
(can_fight weapon)
(guardLoc l12)
(safeLoc l33)
(trapLoc l23)
(gameWinLoc front_house)

(can_unlock_safe safe_code)

(can_disable_trap weapon)


)

(:goal 
		(game_won)
		

)

(:metric minimize(total-cost))

)