(define (domain game)



(:requirements :strips )



(:predicates



(right ?from ?to)

(up ?from ?to)

(down ?from ?to)

(left ?from ?to)



(can_move ?who)



(at ?who ?where)





(pick ?who ?what)

(has ?who ?what)





(door ?from ?to)

(can_unlock ?with)





(road ?from ?to ?with)

(drivable ?what)





(leave_store ?from ?to)

(can_trade ?using)



(for_sale ?what)

(bought ?who ?what)



(can_dig ?with)

(treasure_loc ?from ?to)



(can_fight ?with)

(guardLoc ?where)

(guard_dead)





(can_disable_trap ?with)

(trap_disabled)

(trapLoc ?where)



(can_unlock_safe ?with)

(safeLoc ?where)



(gameWinLoc ?where)



(game_won)





)





(:functions



(obj_weight ?obj) - number



(max_carry ?with) - number



(total-cost) - number

(carrying) - number



(carries ?who) - number



)



(:action move_up

:parameters(?who ?from ?to)



:precondition(and (up ?from ?to)

				(at ?who ?from)

				(can_move ?who)



)

:effect (and ( not (at ?who ?from))

	 		(at ?who ?to)

	 		(increase (total-cost) 1)

	 		)

)





(:action move_right

:parameters(?who ?from ?to)



:precondition(and (right ?from ?to)

				(at ?who ?from)

				(can_move ?who)

)



:effect (and ( not (at ?who ?from))

	 		(at ?who ?to)

	 		(increase (total-cost) 1)



	 		)

)





(:action move_left

:parameters(?who ?from ?to)



:precondition(and (left ?from ?to)

				(at ?who ?from)

				(can_move ?who)

)



:effect (and ( not (at ?who ?from))

	 		(at ?who ?to)

	 		(increase (total-cost) 1)



	 		)

)





(:action move_down

:parameters(?who ?from ?to)



:precondition(and (down ?from ?to)

				(at ?who ?from)

				(can_move ?who)

)



:effect (and ( not (at ?who ?from))

	 		(at ?who ?to)

	 		(increase (total-cost) 1)



	 		)

)



;ridic obiect

(:action pick_obj

:parameters(?who ?from ?what)



:precondition(and (at ?who ?from)  ;sa fiu la pozitia obiectului

				(at ?what ?from) ;sa fie si obiectul la pozitie



				)	



:effect (and (not (at ?what ?from))  ;obiectul dispare

			(has ?who ?what)		;eu am obiectul

			(increase (total-cost) (obj_weight ?what) )



			)		

)



(:action leave_house



:parameters(?who ?from ?to ?using)



:precondition(and

				 (door ?from ?to)

				(at ?who ?from)

				(has ?who ?using)

				(can_move ?who)



				(can_unlock ?using)





				)





:effect (and ( not (at ?who ?from))

				(at ?who ?to)

	 	(increase (total-cost)  1 )



 		)



) 





(:action drive



:parameters(?who ?from ?to ?with)





:precondition(and (road ?from ?to ?with)

				(drivable ?with)

				(at ?with ?from)

				(can_move ?who)

				(at ?who ?from)



)



:effect (and ( not (at ?who ?from))

	 		(at ?who ?to)

	 		( not (at ?with ?from))

	 		(at ?with ?to)

	 		(increase (total-cost)  (obj_weight ?with) )

	 	) 	

)



(:action buy

:parameters (?who ?from ?what ?using)



:precondition( and 

				(at ?who ?from)

				(at ?what ?from)

				(has ?who ?using)

				(can_trade ?using)

				(for_sale ?what)

				;(has ?who ?what)

)

:effect (and 

			(bought ?who ?what)

			(has ?who ?what)

			(increase (total-cost) 1)	



				)

)





(:action leave_store

:parameters (?who ?from ?to ?with)



:precondition( and

			(at ?who ?from)

			(bought ?who ?with)

			(leave_store ?from ?to)



)



:effect ( and

			(not (at ?who ?from))

			(at ?who ?to)

			(increase (total-cost) (obj_weight ?with))



)

)



(:action dig

:parameters (?who ?from ?to ?with)



:precondition( and

				(at ?who ?from)

				(has ?who ?with)

				(can_dig ?with)

				(treasure_loc ?from ?to)

)

:effect(and (not (at ?who ?from))

			(at ?who ?to)

			(increase (total-cost) 1)



)

)



(:action fight_guard

:parameters(?who ?where ?with)



:precondition( and

				  (has ?who ?with)

				 (can_fight ?with )

				 (at ?who ?where)

				 (guardLoc ?where)



)

:effect( and (guard_dead)

		(increase (total-cost) 1)



		)

)



(:action disable_trap



:parameters (?who ?where ?with)



:precondition( and

				(has ?who ?with)

				(can_disable_trap ?with)

				(at ?who ?where)

				(trapLoc ?where)

)



:effect( and ( trap_disabled)

			(increase (total-cost) 1)



		)









)







(:action unlock_safe



:parameters (?who ?where ?with)



:precondition(and

				(has ?who ?with)

				(can_unlock_safe ?with)

				(at ?who ?where)

				(safeLoc ?where)

				(trap_disabled)

				(guard_dead)

			)



:effect( and ( has ?who treasure)

			(increase (total-cost) 1)



		)



)



(:action leave_labirint



:parameters (?who ?from ?to)



:precondition (and 

				(at ?who ?from)

				(left ?from ?to)

				(guard_dead)

				)

:effect(and (not (at ?who ?from))

			(at ?who ?to)

			(increase (total-cost) 1)

		)



)



(:action win_game



:parameters (?who ?where)



:precondition (and



			(at ?who ?where)

			(gameWinLoc ?where)

			(has ?who treasure)





)

:effect( and (game_won)

		(increase (total-cost) 1)



)



)





)